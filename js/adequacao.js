
//OUTPUT CHOSEN AMOUNT
let range = document.getElementById("valorSimulacao");
let output = document.getElementById("valor");
output.innerHTML = range.value;

range.oninput = function () {
    output.innerHTML = this.value;
};

//OUTPUT CHOSEN INSTALLMENT (COMPOUND INTEREST)
let range1 = document.getElementById("qtdParcelas");
let output1 = document.getElementById("valorParcelas");
output1.innerHTML = range1.value;

range1.oninput = function() {
    output1.innerHTML = this.value;
};

// LOAN CALCULATOR

function calcular() {
    let montante = (range.value * 1000) * Math.pow( 1 + 0.01, range1.value); 
    let valorParcela = montante / range1.value;

    let parcelaShow = document.getElementById("parcelaShow").innerHTML = valorParcela.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });  
   
    let montanteShow = document.getElementById("totalPrazoShow").innerHTML = montante.toLocaleString('pt-BR',{style:'currency', currency:'BRL'});    
};
addEventListener("load", calcular())




